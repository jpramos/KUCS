
from KUCS_Classifier import Classifier

import logging

LOGGER = logging.getLogger(__name__)

class ClassifiersSet:
    def __init__(self):

        self.set = []
        

    def empty(self):
        self.set = []


    def isempty(self):
        return len(self.set) == 0

    def get_acc_avg(self):

        whole_acc = 0
        for cl in self.set:
            whole_acc += cl.acc

        return whole_acc / len(self.set)

    def get_error_avg(self):

        whole_err = 0
        for cl in self.set:
            whole_err += (1 - cl.acc)

        return whole_err / len(self.set)


    def print_set(self):

        if self.isempty():
            return None

        LOGGER.debug(self.set[0].print_cl(True))
        for cl in self.set[1:]:
            LOGGER.debug(cl.print_cl(False))


        return None


    def write_set_to_file(self, file_name):

        if self.isempty():
            return None

    
        with open(file_name, 'wb') as fp:
            
            fp.write(self.set[0].print_cl(True) + '\n')
            for cl in self.set[1:]:
                fp.write(cl.print_cl(False) + '\n')


        return None
        

    def write_conditions_to_file(self, file_name):

        if self.isempty():
            return None

        with open(file_name, 'wb') as fd:

            for cl in self.set:
                fd.write(' '.join(map(str, cl.condition)))
                fd.write('\t{}\t{}\t{}'.format(cl.action, cl.exp, cl.acc))
                fd.write('\n')


        return None
