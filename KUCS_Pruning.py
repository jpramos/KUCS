
from KUCS_Constants import *
from KUCS_OfflineEnvironment import OfflineEnvironment
from KUCS_MatchSet import MatchSet
#from KUCS_PopulationSet import PopulationSet
from KUCS_PopulationSet import *
from KUCS_Classifier import *

import KUCS_DataParser as dp
import numpy as np
import pandas as pd

import pickle as pck
import KUCS_Diversity as div_measures
import copy as cp

import matplotlib.pyplot as plt
import matplotlib.patches as patches

import os 

POPULATION_FILE = 'pop.pickle'
TRAINING_FILE = 'train_dta.pickle'
TESTING_FILE = 'test_dta.pickle'

#POPULATION_FILE = 'pop_infarctionA.pickle'
#TRAINING_FILE = 'train_dta_infarctionA.pickle'
#TESTING_FILE = 'test_dta_infarctionA.pickle'
plt.ion()

CONFIG_FILE = 'KUCS_CONFIG.txt'

def evaluate(pop, test_set):

        num_tests = len(test_set)

        y_true = []
        y_score = []
        covered_count = 0
        for x in range(num_tests):

            inst = test_set.ix[x,:]
            
            m_set = pop.form_eval_match_set(inst)
            (action, is_covered) = m_set.cast_votes()

            if is_covered:
                covered_count += 1

            y_true.append(inst[len(inst)-1])
            y_score.append(float(action))


        tp = 0
        tn = 0
        fp = 0
        fn = 0
        for (t, p) in zip(y_true, y_score):
            if t:
                if t == p:
                    tp += 1
                else:
                    fn += 1
            else:
                if t == p:
                    tn += 1
                else:
                    fp += 1

        sens = tp / float(tp + fn)
        spec = tn / float(tn + fp)
        acc = (tp + tn) / float(tp + fn + tn + fp)
        gmean = (sens * spec) ** (1/2.0)

        return (gmean, acc, sens, spec)

def read_config_file(config_file):

    comment_char = '%'
    param_char = '='

    parameters = {}

    with open(config_file, 'r') as f:
        for rline in f:

            # remove comments in line
            if comment_char in rline:
                rline, comment = rline.split(comment_char, 1)

            # get parameters and their values
            if param_char in rline:
                param, value = rline.split(param_char, 1)
                param = param.strip()
                value = value.strip()
                parameters[param] = value

    cons.set_constants(parameters)


def read_dump_file(dump_file):
    
    d_obj = pck.load(file(dump_file))

    return d_obj

def eval_sets(pop, eval_set):

    for cl in pop.set:
        cl.correctness = [0] * len(eval_set)

    for (inst_c, inst) in eval_set.iterrows():

        for cl in pop.set:

            if cl.match(inst):
                cl.correctness[inst_c] = 1
            else:
                cl.correctness[inst_c] = 0

    return None

def pop_pruning(pop, training_set):

    eval_sets(pop, training_set)

    #(df_avg, single_df) = div_measures.avg_double_fault(pop)
    #(df_avg, single_df) = div_measures.avg_disagreement(pop)
    (df_avg, single_df) = div_measures.avg_q_statistics(pop)

    return (df_avg, single_df)


def eval_s(pop, training_set):

    eval_sets(pop, training_set)

    return None

def match_set_pruning(m_set):

    (q_avg, paired_q) = div_measures.avg_q_statistics(m_set)

    m_val = min(zip(*paired_q)[2])
    to_keep_ind = []
    to_lose_ind = []
    for pair in paired_q:
        if pair[2] <= m_val:
            if pair[0] in to_lose_ind or pair[1] in to_lose_ind:
                continue
            if m_set.set[pair[0]].F_macro > m_set.set[pair[1]].F_macro:
                to_keep_ind.append(pair[0])
            else:
                to_keep_ind.append(pair[1])
        else:
            #do not add both
            if m_set.set[pair[0]].F_macro < m_set.set[pair[1]].F_macro:
                to_lose_ind.append(pair[0])
            else:
                to_lose_ind.append(pair[1])

    #(gmean, acc, se, sp) = evaluate(pop, testing_set)

    new_pop = MatchSet()
    [new_pop.set.append(item) for i, item in enumerate(m_set.set) if i in
            to_keep_ind]


    return new_pop

def find_element_diversity(pop, paired_q):

    div_sum = [0]*len(pop.set)
    for (index, individual) in enumerate(pop.set):

        for pair in paired_q:
            if pair[0] == index:
                div_sum[index] += pair[2]
            elif pair[1] == index:
                div_sum[index] += pair[2]


    return div_sum


def split_div_maximization(pop, training_set, testing_set, verbose=0):

    if verbose:
        train_X = training_set.values[:,0:2]
        train_y = training_set.values[:,2]

        t_figure = plt.figure()
        ax = plt.subplot(1,1,1)
        ax.scatter(train_X[:,0], train_X[:,1], c=train_y)



    pos_pop = PopulationSet(0)
    [pos_pop.insert(item) for item in pop.set if item.action == 1]

    eval_sets(pos_pop, training_set)

    (pos_q_avg, pos_paired_q) = div_measures.avg_q_statistics(pos_pop)

    pos_div_sum = find_element_diversity(pos_pop, pos_paired_q)


    neg_pop = PopulationSet(0)
    [neg_pop.insert(item) for item in pop.set if item.action == 0]
    
    eval_sets(neg_pop, training_set)

    (neg_q_avg, neg_paired_q) = div_measures.avg_q_statistics(neg_pop)
    
    neg_div_sum = find_element_diversity(neg_pop, neg_paired_q)


    return None



def diversity_maximization(pop, training_set, testing_set, verbose=0):

    #if os.path.exists('div_calc.pickle'):
    #    (q_avg, paired_q) = pck.load(file('div_calc.pickle'))
    #else:
    eval_sets(pop, training_set)

    (q_avg, paired_q) = div_measures.avg_q_statistics(pop)

    #    pck.dump((q_avg, paired_q), open('div_calc.pickle', 'wb'))

    if verbose:
        train_X = training_set.values[:,0:2]
        train_y = training_set.values[:,2]

        t_figure = plt.figure()
        ax = plt.subplot(1,1,1)
        ax.scatter(train_X[:,0], train_X[:,1], c=train_y)


    
    div_sum = find_element_diversity(pop, paired_q)

    so_div_sum = [i for i in sorted(enumerate(div_sum), key=lambda x:x[1])]


    new_pop = PopulationSet(0)
    [new_pop.insert(item) for i, item in enumerate(pop.set) if
            div_sum[i] < 0]


    if verbose:
        
        for pair in paired_q:
            
            rule1 = pop.set[pair[0]].condition
            rule2 = pop.set[pair[1]].condition

            x1_bound = rule1[0]
            if x1_bound == '#':
                x1_bound = ax.get_xlim()
            y1_bound = rule1[1]
            if y1_bound == '#':
                y1_bound = ax.get_ylim()

            x1 = min(x1_bound)
            y1 = min(y1_bound)

            width1 = abs(x1_bound[1] - x1_bound[0])
            height1 = abs(y1_bound[1] - y1_bound[0])
            
            if pop.set[pair[0]].action == 1:
                c = '#FF0000'
            else:
                c = '#0000FF'
            pr1 = patches.Rectangle((x1, y1), width1, height1, alpha=.1,
                    color=c)
            ax.add_patch(pr1)


            x2_bound = rule2[0]
            if x2_bound == '#':
                x2_bound = ax.get_xlim()
            y2_bound = rule2[1]
            if y2_bound == '#':
                y2_bound = ax.get_ylim()

            x2 = min(x2_bound)
            y2 = min(y2_bound)

            width2 = abs(x2_bound[1] - x2_bound[0])
            height2 = abs(y2_bound[1] - y2_bound[0])
            
            if pop.set[pair[1]].action == 1:
                c = '#FF0000'
            else:
                c = '#0000FF'
            pr2 = patches.Rectangle((x2, y2), width2, height2, alpha=.1,
                    color=c)
            ax.add_patch(pr2)

            print("pair diversity {} / {} diversity {} / {} diversity {}\n".format(pair[2], pair[0], div_sum[pair[0]], pair[1], div_sum[pair[1]]))
            plt.show()
            pr1.remove()
            pr2.remove()




    (g, a, s, p) = evaluate(new_pop, testing_set)

    with open("eval_with_pruning.txt", "a") as fd:
            fd.write("{}\t{}\t{}\t{}\n".format(a, s, p, g))

    with open('pop_with_pruning.txt', 'a') as fd:
        fd.write('{}\n'.format(len(new_pop.set)))

    pck.dump(new_pop, open('pop_pruned.pickle', 'wb'))


    return None


def p_pr(pop=None, training_set=None, testing_set=None, verbose=0):

    if verbose:
        train_X = training_set.values[:,0:2]
        train_y = training_set.values[:,2]

        t_figure = plt.figure()
        ax = plt.subplot(1,1,1)
        ax.scatter(train_X[:,0], train_X[:,1], c=train_y)



    pos_pop = PopulationSet(0)
    [pos_pop.insert(item) for item in pop.set if item.action == 1]
    eval_sets(pos_pop, training_set)
    (pos_q_avg, pos_paired_q) = div_measures.avg_q_statistics(pos_pop)

    sel_pairs = [p for p in pos_paired_q if p[2] == -1]

    sel_inds = []
    [sel_inds.extend([sp[0], sp[1]]) for sp in sel_pairs]
    #for sp in sel_pairs:
    #    sel_inds.append(sp[0])
    #    sel_inds.append(sp[1])

    n_sel_pairs = [p for p in pos_paired_q if p[2] == 1]
    n_sel_inds = []
    [n_sel_inds.extend([sp[0], sp[1]]) for sp in n_sel_pairs]
    prospects = [x for x in set(n_sel_inds) if x not in set(sel_inds)]

    new_pos_pop = PopulationSet(0)
    [new_pos_pop.insert(item) for i, item in enumerate(pos_pop.set) if i in
            prospects]


    neg_pop = PopulationSet(0)
    [neg_pop.insert(item) for item in pop.set if item.action == 0]
    eval_sets(neg_pop, training_set)
    (neg_q_avg, neg_paired_q) = div_measures.avg_q_statistics(neg_pop)

    sel_pairs = [p for p in neg_paired_q if p[2] == -1]
    sel_inds = []
    [sel_inds.extend([sp[0], sp[1]]) for sp in sel_pairs]
    #for sp in sel_pairs:
    #    sel_inds.append(sp[0])
    #    sel_inds.append(sp[1])

    n_sel_pairs = [p for p in neg_paired_q if p[2] == 1]
    n_sel_inds = []
    [n_sel_inds.extend([sp[0], sp[1]]) for sp in n_sel_pairs]
    prospects = [x for x in set(n_sel_inds) if x not in set(sel_inds)]

    new_neg_pop = PopulationSet(0)
    [new_neg_pop.insert(item) for i, item in enumerate(neg_pop.set) if i in
            prospects]

    new_pop = PopulationSet(0)
    [new_pop.insert(item) for item in new_pos_pop.set]
    [new_pop.insert(item) for item in new_neg_pop.set]


    if verbose:
        #train_X = training_set.values[:,0:2]
        #train_y = training_set.values[:,2]

        #t_figure = plt.figure()
        #ax = plt.subplot(1,1,1)
        #ax.scatter(train_X[:,0], train_X[:,1], c=train_y)

        #plt.show()


        for r in new_pop.set:

            rule = r.condition

            x_bound = rule[0]
            if x_bound == '#':
                x_bound = ax.get_xlim()
            y_bound = rule[1]
            if y_bound == '#':
                y_bound = ax.get_ylim()

            x = min(x_bound)
            y = min(y_bound)

            width = abs(x_bound[1] - x_bound[0])
            height = abs(y_bound[1] - y_bound[0])

            if r.action == 1:
                c = '#FF0000'
            else:
                c = '#0000FF'

            p = patches.Rectangle((x,y), width, height, alpha=.1, color=c)
            ax.add_patch(p)

    (g, a, s, p) = evaluate(new_pop, testing_set)

    with open("eval_with_pruning.txt", "a") as fd:
            fd.write("{}\t{}\t{}\t{}\n".format(a, s, p, g))

    with open('pop_with_pruning.txt', 'a') as fd:
        fd.write('{}\n'.format(len(new_pop.set)))

    pck.dump(new_pop, open('pop_pruned.pickle', 'wb'))



    return None
     
def pop_pr(pop=None, training_set=None, testing_set=None, verbose=0):

    #if os.path.exists('div_calc.pickle'):
    #    (q_avg, paired_q) = pck.load(file('div_calc.pickle'))
    #else:

    if verbose:
        train_X = training_set.values[:,0:2]
        train_y = training_set.values[:,2]

        t_figure = plt.figure()
        ax = plt.subplot(1,1,1)
        ax.scatter(train_X[:,0], train_X[:,1], c=train_y)



    pos_pop = PopulationSet(0)
    [pos_pop.insert(item) for item in pop.set if item.action == 1]

    eval_sets(pos_pop, training_set)

    (pos_q_avg, pos_paired_q) = div_measures.avg_q_statistics(pos_pop)

    to_lose_ind = []
    for pair in pos_paired_q:
        if pair[0] in to_lose_ind:
            continue
        if pair[2] > 0:
            el1_gmean = pos_pop.set[pair[0]].exp*.5 + pos_pop.set[pair[0]].F_macro*.5
            el2_gmean = pos_pop.set[pair[1]].exp*.5 + pos_pop.set[pair[1]].F_macro*.5
            if el1_gmean > el2_gmean:
            #if pos_pop.set[pair[0]].exp > pos_pop.set[pair[1]].exp:
                to_lose_ind.append(pair[1])
            else:
                to_lose_ind.append(pair[0])

    new_pos_pop = PopulationSet(0)
    [new_pos_pop.insert(item) for i, item in enumerate(pos_pop.set) if i not in
            to_lose_ind]

    #############################################3
    # negative population calculation
    ###############################################

    neg_pop = PopulationSet(0)
    [neg_pop.insert(item) for item in pop.set if item.action == 0]
    
    eval_sets(neg_pop, training_set)

    (neg_q_avg, neg_paired_q) = div_measures.avg_q_statistics(neg_pop)

    to_lose_ind = []
    for pair in neg_paired_q:
        if pair[0] in to_lose_ind:
            continue
        if pair[2] > 0:
            el1_gmean = neg_pop.set[pair[0]].exp*.5 + neg_pop.set[pair[0]].F_macro*.5
            el2_gmean = neg_pop.set[pair[1]].exp*.5 + neg_pop.set[pair[1]].F_macro*.5
            if el1_gmean > el2_gmean:
            #if neg_pop.set[pair[0]].exp > neg_pop.set[pair[1]].exp:
                to_lose_ind.append(pair[1])
            else:
                to_lose_ind.append(pair[0])

    new_neg_pop = PopulationSet(0)
    [new_neg_pop.insert(item) for i, item in enumerate(neg_pop.set) if i not in
            to_lose_ind]

    new_pop = PopulationSet(0)
    [new_pop.insert(item) for item in new_pos_pop.set]
    [new_pop.insert(item) for item in new_neg_pop.set]

    #eval_sets(pop, training_set)

    #(q_avg, paired_q) = div_measures.avg_q_statistics(pop)

    ##    pck.dump((q_avg, paired_q), open('div_calc.pickle', 'wb'))


    #to_lose_ind = []
    #for pair in paired_q:
    #    if pair[0] in to_lose_ind:
    #        continue
    #    if pair[2] > 0:
    #        #el1_gmean = (pop.set[pair[0]].exp**2 *
    #        #        pop.set[pair[0]].F_macro**2)**.5
    #        #el2_gmean = (pop.set[pair[1]].exp**2 *
    #        #        pop.set[pair[1]].F_macro**2)**.5
    #        el1_gmean = pop.set[pair[0]].exp*.5 + pop.set[pair[0]].F_macro*.5
    #        el2_gmean = pop.set[pair[1]].exp*.5 + pop.set[pair[1]].F_macro*.5
    #        if el1_gmean > el2_gmean:
    #        #if pop.set[pair[0]].F_macro > pop.set[pair[1]].F_macro:
    #            to_lose_ind.append(pair[1])
    #        else:
    #            to_lose_ind.append(pair[0])

    #new_pop = PopulationSet(0)
    #[new_pop.insert(item) for i, item in enumerate(pop.set) if i not in
    #        to_lose_ind]

    (g, a, s, p) = evaluate(new_pop, testing_set)

    if verbose:
        #train_X = training_set.values[:,0:2]
        #train_y = training_set.values[:,2]

        #t_figure = plt.figure()
        #ax = plt.subplot(1,1,1)
        #ax.scatter(train_X[:,0], train_X[:,1], c=train_y)

        #plt.show()


        for r in new_pop.set:

            rule = r.condition

            x_bound = rule[0]
            if x_bound == '#':
                x_bound = ax.get_xlim()
            y_bound = rule[1]
            if y_bound == '#':
                y_bound = ax.get_ylim()

            x = min(x_bound)
            y = min(y_bound)

            width = abs(x_bound[1] - x_bound[0])
            height = abs(y_bound[1] - y_bound[0])

            if r.action == 1:
                c = '#FF0000'
            else:
                c = '#0000FF'

            p = patches.Rectangle((x,y), width, height, alpha=.1, color=c)
            ax.add_patch(p)
        
        #rule1 = new_pop.set[0].condition
        #rule2 = new_pop.set[1].condition

        #x1_bound = rule1[0]
        #if x1_bound == '#':
        #    x1_bound = ax.get_xlim()
        #y1_bound = rule1[1]
        #if y1_bound == '#':
        #    y1_bound = ax.get_ylim()

        #x1 = min(x1_bound)
        #y1 = min(y1_bound)

        #width1 = abs(x1_bound[1] - x1_bound[0])
        #height1 = abs(y1_bound[1] - y1_bound[0])
        #
        #if new_pop.set[0].action == 1:
        #    c = '#FF0000'
        #else:
        #    c = '#0000FF'
        #pr1 = patches.Rectangle((x1, y1), width1, height1, alpha=.1,
        #        color=c)
        #ax.add_patch(pr1)


        #x2_bound = rule2[0]
        #if x2_bound == '#':
        #    x2_bound = ax.get_xlim()
        #y2_bound = rule2[1]
        #if y2_bound == '#':
        #    y2_bound = ax.get_ylim()

        #x2 = min(x2_bound)
        #y2 = min(y2_bound)

        #width2 = abs(x2_bound[1] - x2_bound[0])
        #height2 = abs(y2_bound[1] - y2_bound[0])
        #
        #if new_pop.set[1].action == 1:
        #    c = '#FF0000'
        #else:
        #    c = '#0000FF'
        #pr2 = patches.Rectangle((x2, y2), width2, height2, alpha=.1,
        #        color=c)
        #ax.add_patch(pr2)

        #plt.show()

    with open("eval_with_pruning.txt", "a") as fd:
            fd.write("{}\t{}\t{}\t{}\n".format(a, s, p, g))

    with open('pop_with_pruning.txt', 'a') as fd:
        fd.write('{}\n'.format(len(new_pop.set)))


    pck.dump(new_pop, open('pop_pruned.pickle', 'wb'))




    return None



def population_pruning(pop=None, training_set=None, testing_set=None):


    #if not pop:
    #    pop = read_dump_file(POPULATION_FILE) 

    #if not training_set:
    #    training_set = read_dump_file(TRAINING_FILE)

    #if not testing_set:
    #    testing_set = read_dump_file(TESTING_FILE)


    eval_sets(pop, training_set)

    (q_avg, paired_q) = div_measures.avg_q_statistics(pop)
    #(df_avg, single_df) = div_measures.avg_double_fault(pop)

    m_val = min(zip(*paired_q)[2])
    to_keep_ind = []
    to_lose_ind = []
    for pair in paired_q:
        if pair[2] <= m_val:
            if pair[0] in to_lose_ind or pair[1] in to_lose_ind:
                continue
            if pop.set[pair[0]].exp > pop.set[pair[1]].exp:
                to_keep_ind.append(pair[0])
            else:
                to_keep_ind.append(pair[1])
        else:
            #do not add both
            if pop.set[pair[0]].exp < pop.set[pair[1]].exp:
                to_lose_ind.append(pair[0])
            else:
                to_lose_ind.append(pair[1])

    (gmean, acc, se, sp) = evaluate(pop, testing_set)

    new_pop = PopulationSet(0)
    [new_pop.insert(item) for i, item in enumerate(pop.set) if i in
            to_keep_ind]

    (g, a, s, p) =evaluate(new_pop, testing_set)

    with open("eval_with_pruning.txt", "a") as fd:
            fd.write("{}\t{}\t{}\t{}\n".format(a, s, p, g))

    with open('pop_with_pruning.txt', 'a') as fd:
        fd.write('{}\n'.format(len(new_pop.set)))


    pck.dump(new_pop, open('pop_pruned.pickle', 'wb'))

    #eval_sets(new_pop, training_set)
    #(q, pq) = div_measures.avg_q_statistics(new_pop)

    #new_pop = cp.deepcopy(pop)

    #s_single = sorted(single_df, key=lambda x:x[2], reverse=True)
    #remove_cls = []
    #for comb in s_single:

    #    if comb[2] < .75:
    #        break

    #    cl_1 = pop.set[comb[0]]
    #    cl_2 = pop.set[comb[1]]

    #    if cl_1.F_macro > cl_2.F_macro:
    #        remove_cls.append(comb[1])
    #    else:
    #        remove_cls.append(comb[0])

    #remove_cls = set(remove_cls)

    #new_pop = PopulationSet(0)
    #[new_pop.insert(item) for i, item in enumerate(pop.set) if i not in remove_cls]

    #keep_cls = []
    #for comb in s_single:

    #    if comb[2] < .25:
    #        keep_cls.append(comb[0])
    #        keep_cls.append(comb[1])

    #keep_cls = set(keep_cls)
    #
    #new_pop = PopulationSet(0)
    #[new_pop.insert(item) for i, item in enumerate(pop.set) if i in keep_cls]

    #(g, a, s, p) = evaluate(new_pop, testing_set)
    #(df_avg, single_df) = div_measures.avg_double_fault(new_pop)


    return None

def calc_mult11_div():

    read_config_file(CONFIG_FILE)

    env = OfflineEnvironment()
    cons.env = env


    conditions = [
            [0.0, 0.0, 0.0, 0.0, '#', '#', '#', '#', '#', '#', '#'],
            [0.0, 0.0, 1.0, '#', 0.0, '#', '#', '#', '#', '#', '#'],
            [0.0, 1.0, 0.0, '#', '#', 0.0, '#', '#', '#', '#', '#'],
            [0.0, 1.0, 1.0, '#', '#', '#', 0.0, '#', '#', '#', '#'],
            [1.0, 0.0, 0.0, '#', '#', '#', '#', 0.0, '#', '#', '#'],
            [1.0, 0.0, 1.0, '#', '#', '#', '#', '#', 0.0, '#', '#'],
            [1.0, 1.0, 0.0, '#', '#', '#', '#', '#', '#', 0.0, '#'],
            [1.0, 1.0, 1.0, '#', '#', '#', '#', '#', '#', '#', 0.0],
            [0.0, 0.0, 0.0, 1.0, '#', '#', '#', '#', '#', '#', '#'],
            [0.0, 0.0, 1.0, '#', 1.0, '#', '#', '#', '#', '#', '#'],
            [0.0, 1.0, 0.0, '#', '#', 1.0, '#', '#', '#', '#', '#'],
            [0.0, 1.0, 1.0, '#', '#', '#', 1.0, '#', '#', '#', '#'],
            [1.0, 0.0, 0.0, '#', '#', '#', '#', 1.0, '#', '#', '#'],
            [1.0, 0.0, 1.0, '#', '#', '#', '#', '#', 1.0, '#', '#'],
            [1.0, 1.0, 0.0, '#', '#', '#', '#', '#', '#', 1.0, '#'],
            [1.0, 1.0, 1.0, '#', '#', '#', '#', '#', '#', '#', 1.0],
            ['#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#']]


    actions = [0]*8
    for _ in range(8):
        actions.append(1)

    actions.append(1)
    pop = PopulationSet(0)
    for (c, a) in zip(conditions, actions):
        tmp = Classifier()
        tmp.create_match(c, a)
        pop.insert(tmp)

    
    vals = dp.read_data('input_data/multiplexer_11.data')
    #header = vals[0, :]
    #vals = np.delete(vals, (0), axis=0)

    d_set = pd.DataFrame(vals)

    (g, a, s, p) = evaluate(pop, d_set)
    eval_sets(pop, d_set)
    (df_avg, paired_df) = div_measures.avg_double_fault(pop)
    (q_avg, paired_q) = div_measures.avg_q_statistics(pop)
    (dis_avg, paired_dis) = div_measures.avg_disagreement(pop)

    pass


def test_pruning():

    read_config_file(CONFIG_FILE)

    env = OfflineEnvironment()
    cons.env = env

    pop = read_dump_file(POPULATION_FILE) 

    training_set = read_dump_file(TRAINING_FILE)

    testing_set = read_dump_file(TESTING_FILE)


    #population_pruning(pop, training_set, testing_set)
    #pop_pr(pop, training_set, testing_set, verbose=1)
    p_pr(pop, training_set, testing_set, verbose=1)

    #pop_pruning(pop, training_set)


def eval_diversity_maximization():

    read_config_file(CONFIG_FILE)
    env = OfflineEnvironment()
    cons.env = env

    pop = read_dump_file(POPULATION_FILE)
    training_set = read_dump_file(TRAINING_FILE)
    testing_set = read_dump_file(TESTING_FILE)

    diversity_maximization(pop, training_set, testing_set, verbose=1)

    #split_div_maximization(pop, training_set, testing_set, verbose=1)

if __name__ == '__main__':

    test_pruning()
    #eval_diversity_maximization()
    #calc_mult11_div()
