
#Import Required Modules--------
from sklearn.cross_validation import StratifiedShuffleSplit
from sklearn.cross_validation import ShuffleSplit
import random
import numpy as np
import pandas as pd
import re
import shutil as sh
import os

OUT_PATH = 'output_data/'
IN_PATH = 'input_data/'
#DATASET = 'checkerboard_s4096_c4_i0'
DATASET = 'infarctionA'
#DATASET = 'arrhythmia'
N_ITERATIONS = 10
CONFIG_FILE = 'KUCS_CONFIG.txt'

def create_cross_val_sets(data_file, n_it):

    vals = read_data(data_file)
    header = vals[0,:]
    vals = np.delete(vals, (0), axis=0)

    #sss = StratifiedShuffleSplit(vals[:,-1], test_size=.3, random_state=42,
            #n_iter=n_it)
    sss = ShuffleSplit(len(vals[:,-1]), n_iter=n_it, test_size=.3,
            random_state=42)

    iter = 0
    for train_index, test_index in sss:
        x_train, x_test = vals[train_index], vals[test_index]

        x1_train = oversampling(x_train)

        train_positive = np.sum(np.array(x1_train[:,-1]) == np.array(['1']))
        test_positive = np.sum(np.array(x_test[:,-1]) == np.array(['1']))

        print("train ratio: {} / {} \t test ratio: {} / {}".format(train_positive, len(x1_train[:-1]), test_positive, len(x_test[:,-1])))

        dta_train = pd.DataFrame(x1_train)
        dta_train.to_csv(OUT_PATH + DATASET + '_' + str(iter) + '_train.txt',
        sep=',', index=False, header=False)

        dta_test = pd.DataFrame(x_test)
        dta_test.to_csv(OUT_PATH + DATASET + '_' + str(iter) + '_test.txt',
                sep=',', index=False, header=False)
        
        iter += 1 

    return 0



def oversampling(a):

    unq, unq_idx = np.unique(a[:, -1], return_inverse=True)
    unq_cnt = np.bincount(unq_idx)
    #unq_cnt[0] = round(unq_cnt[0] * .75) # remove this line if you equals sizes
    cnt = np.max(unq_cnt)
    tmp = sum(np.cumsum(cnt - unq_cnt))
    out = np.empty((tmp,) + a.shape[1:], a.dtype)
    slices = np.concatenate(([0], np.cumsum(cnt - unq_cnt)))
    
    for j in xrange(len(unq)):
        indices = np.random.choice(np.where(unq_idx==j)[0], cnt - unq_cnt[j])
        out[slices[j]:slices[j+1]] = a[indices]

    out = np.vstack((a, out))


    return out


def read_data(data_file):

    vals = np.genfromtxt(data_file, delimiter=',', missing_values='?',
            dtype=np.str)

    return vals


if __name__ == '__main__':

    N_INIT = 100
    N_STEP = 100
    N_STOP = 2000

    P_WD_INIT = 0
    P_WD_STEP = 1
    P_WD_STOP = 11

    for n in range(N_INIT, N_STOP, N_STEP):

        with open(CONFIG_FILE, 'r+') as n_c_file:
            n_content = n_c_file.read()
            new_n_content = re.sub('(?<=N\=)\w+', str(n), n_content)
            n_c_file.seek(0)
            n_c_file.write(new_n_content)


        for p_wd in range(P_WD_INIT, P_WD_STOP, P_WD_STEP):
            
            with open(CONFIG_FILE, 'r+') as p_c_file:
                p_content = p_c_file.read()
                new_p_content = re.sub('(?<=p_WD\=)([0-9]*\.[0-9]*)', str(p_wd/10.0), p_content)
                p_c_file.seek(0)
                p_c_file.write(new_p_content)

            create_cross_val_sets(IN_PATH + DATASET + '.data', N_ITERATIONS)

            for it in xrange(N_ITERATIONS):

                with open(CONFIG_FILE, 'r+') as content_file:
                    content = content_file.read()
                    # copy train data
                    train_f = DATASET + '_' + str(it) + '_train.txt'
                    sh.copy(OUT_PATH + train_f, '.')
                    new_content = re.sub('(?<=trainFile\=)\w+\.txt', train_f, content)

                    # copy test data
                    test_f = DATASET + '_' + str(it) + '_test.txt'
                    sh.copy(OUT_PATH + test_f, '.')
                    new_content = re.sub('(?<=testFile\=)\w+(\.txt)*', test_f, new_content)

                    content_file.seek(0)
                    content_file.write(new_content)


                os.system('python2 KUCS_Run.py')


