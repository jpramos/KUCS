"""
Name:        KXCS_Rule.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
Portugal
Contact:     jpramos@dei.uc.pt
Created:     September 29, 2015
Description: This module defines a pre-defined expert rule. 

---------------------------------------------------------------------------------------------------------------------------------------------------------
eLCS: Educational Learning Classifier System - A basic LCS coded for educational purposes.  This LCS algorithm uses supervised learning, and thus is most 
similar to "UCS", an LCS algorithm published by Ester Bernado-Mansilla and Josep Garrell-Guiu (2003) which in turn is based heavily on "XCS", an LCS 
algorithm published by Stewart Wilson (1995).  

Copyright (C) 2013 Ryan Urbanowicz 
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the 
Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABLILITY 
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, 
Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
---------------------------------------------------------------------------------------------------------------------------------------------------------
"""

#Import Required Modules---------------
from KUCS_Constants import * 
import random
import sys
import math
#--------------------------------------

class Rule:
    def __init__(self, att_ref=None,  att=None, th=None, phenot=None,
            conf_l=None):
        # rule should follow the following template
        # attribute <cond> threshold then phenotype

        self.att_ref = att_ref
        self.phenotype = phenot # class (phenotype) given by the rule
        self.attribute = att    # attribute used in the rule

        self.threshold = th     # threshold used in the rule

        self.conf_level = conf_l

    def activate_rule(self, th, max_val, min_val):

        dist = abs(th - self.threshold)

        p_radius = .10
        sig = (float(max_val) - float(min_val)) * p_radius

        if (dist <= sig):

            bias = self.conf_level * math.e ** (-(th-self.threshold)**2/(2*sig**2))
        else:
            bias = 0
        # ret = math.e ** (-(th-self.threshold)**2/(2 * sig**2))
        #ret = math.e ** (-(th-self.threshold)**2/(2 * sig**2)) / (math.sqrt(2 * math.pi) * sig)

        return bias
