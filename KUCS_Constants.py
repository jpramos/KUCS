

class Constants:
    def __init__(self):
        pass

    def set_constants(self, params):

        self.namesFile = params['namesFile']
        self.trainFile = params['trainFile']    # Data and expert knowledge
        self.testFile = params['testFile']
        self.ruleFile = params['ruleFile']

        self.max_iter = float(params['max_iter'])

        self.dont_care_sym = params['dont_care_sym']
        self.label_missing_data = params['label_missing_data']

        self.N = int(params['N'])               # Maximum population size
        self.beta = float(params['beta'])       # Learning rate

        self.alpha = float(params['alpha'])     # used in the fitness
        self.eps_0 = float(params['eps_0'])
        self.nu = float(params['nu'])

        self.gamma = float(params['gamma'])     # discount factor

        self.theta_GA = float(params['theta_GA']) # GA threshold
        self.chi = float(params['chi'])         # probability of crossover
        self.mu = float(params['mu'])           # probability of mutation

        self.theta_Del = float(params['theta_Del'])
        self.delta = float(params['delta'])

        self.theta_Sub = float(params['theta_Sub'])

        self.p_WD = float(params['p_WD'])             # probability of a wild
                                                    # card when covering 

        self.p_I = float(params['p_I'])               # init values for new
        self.eps_I = float(params['eps_I'])           # classifiers
        self.acc_0 = float(params['acc_0'])
        self.f_I = float(params['F_I'])

        self.p_explr = float(params['p_explr'])     # probability during action
                                                    # selection of choosing the action uniform randomly
        
        self.theta_mna = int(params['theta_mna'])

        self.P_sp = float(params['P_sp'])
        self.N_sp = int(params['N_sp'])

        self.do_GA_subsumption = params['do_GA_subsumption'] == 'True'
        self.do_actionset_subsumption = params['do_actionset_subsumption'] == 'True'


        self.env = None

        


cons = Constants()
