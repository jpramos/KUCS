"""
Name:        KXCS_RuleManagement.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
Portugal
Contact:     jpramos@dei.uc.pt
Created:     September 29, 2015
Description: Able to manage pre-defined expert rules. This module loads the
ruleset.
             
---------------------------------------------------------------------------------------------------------------------------------------------------------
eLCS: Educational Learning Classifier System - A basic LCS coded for educational purposes.  This LCS algorithm uses supervised learning, and thus is most 
similar to "UCS", an LCS algorithm published by Ester Bernado-Mansilla and Josep Garrell-Guiu (2003) which in turn is based heavily on "XCS", an LCS 
algorithm published by Stewart Wilson (1995).  

Copyright (C) 2013 Ryan Urbanowicz 
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the 
Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABLILITY 
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, 
Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
---------------------------------------------------------------------------------------------------------------------------------------------------------
"""

#Import Required Modules---------------
from KUCS_Constants import * 
from KUCS_Rule import Rule
import random
import sys
#--------------------------------------


class RuleManagement:
    def __init__(self, ruleFile, att_list):

        self.ruleset = self.loadRules(ruleFile, att_list)



    def loadRules(self, ruleFile, att_list):
        """ Load the rule file. """
        print "RuleManagement: Loading rules... " + str(ruleFile)

        try:
            rule_list = []
            f = open(ruleFile, 'r')

            for line in f:
                r_vals = line.strip('\n').split(' ')
                att = r_vals[1]
                att_ref = att_list.index(att)
                th = float(r_vals[3])
                phenot = float(r_vals[0])
                conf_level = float(r_vals[4])

                rule_list.append(Rule(att_ref, att, th, phenot, conf_level))

        except IOError, (errno, strerror):
            print ("Could not Read File!")
            print ("I/O error(%s): %s" % (errno, strerror))
            raise
        except:
            print ("Unexpected error: ", sys.exc_info()[0])
            raise

        return rule_list


    def check_rules_activation(self, attRef, th):

        if th == cons.label_missing_data:
            return 0

        activated_th = []
        for rule in self.ruleset:
            if rule.att_ref == attRef:
                max_val = cons.env.attribute_info[rule.att_ref]['max_value']
                min_val = cons.env.attribute_info[rule.att_ref]['min_value']
                activated_th.extend([rule.activate_rule(th, max_val, min_val)])


        if not activated_th:
            return 0
        else:
            return max(activated_th)
