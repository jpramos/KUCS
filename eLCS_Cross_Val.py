
#Import Required Modules--------
from sklearn.cross_validation import StratifiedShuffleSplit
from sklearn.cross_validation import ShuffleSplit
import random
import numpy as np
import pandas as pd
import re
import shutil as sh
import os

OUT_PATH = 'output_data/'
IN_PATH = 'input_data/'
#DATASET = 'checkerboard_s4096_c4_i0'
#DATASET = 'infarctionA'
#DATASET = 'multiplexer_11'
DATASET = 'RAND_DATASET'
#DATASET = 'arrhythmia'
N_ITERATIONS = 10
CONFIG_FILE = 'KUCS_CONFIG.txt'

def create_cross_val_sets(data_file, n_it):

    vals = read_data(data_file)
    header = vals[0,:]
    vals = np.delete(vals, (0), axis=0)

    #sss = StratifiedShuffleSplit(vals[:,-1], test_size=.3, random_state=42,
            #n_iter=n_it)
    sss = ShuffleSplit(len(vals[:,-1]), n_iter=n_it, test_size=.3,
            random_state=42)

    iter = 0
    for train_index, test_index in sss:
        x_train, x_test = vals[train_index], vals[test_index]

        train_positive = np.sum(np.array(x_train[:,-1]) == np.array(['1']))
        test_positive = np.sum(np.array(x_test[:,-1]) == np.array(['1']))

        print("train ratio: {} / {} \t test ratio: {} / {}".format(train_positive, len(x_train[:-1]),
                    test_positive, len(x_test[:,-1])))

        dta_train = pd.DataFrame(x_train)
        dta_train.to_csv(OUT_PATH + DATASET + '_' + str(iter) + '_train.txt',
        sep=',', index=False, header=False)

        dta_test = pd.DataFrame(x_test)
        dta_test.to_csv(OUT_PATH + DATASET + '_' + str(iter) + '_test.txt',
                sep=',', index=False, header=False)
        
        iter += 1 

    return 0

def read_data(data_file):

    vals = np.genfromtxt(data_file, delimiter=',', missing_values='?',
            dtype=np.str)

    return vals


if __name__ == '__main__':

    create_cross_val_sets(IN_PATH + DATASET + '.data', N_ITERATIONS)

    for it in xrange(N_ITERATIONS):

        with open(CONFIG_FILE, 'r+') as content_file:
            content = content_file.read()
            # copy train data
            train_f = DATASET + '_' + str(it) + '_train.txt'
            sh.copy(OUT_PATH + train_f, '.')
            new_content = re.sub('(?<=trainFile\=)\w+\.txt', train_f, content)

            # copy test data
            test_f = DATASET + '_' + str(it) + '_test.txt'
            sh.copy(OUT_PATH + test_f, '.')
            new_content = re.sub('(?<=testFile\=)\w+(\.txt)*', test_f, new_content)

            content_file.seek(0)
            content_file.write(new_content)


        os.system('python2 KUCS_Run.py')
        os.system('python2 plot_hypspace.py')


