

from KUCS_Constants import *
from KUCS_Environment import Environment
from KUCS_RuleManagement import RuleManagement
import KUCS_DataParser as dp
import pandas as pd
import itertools as it
import random as rand

class OfflineEnvironment(Environment):
    def __init__(self):

        Environment.__init__(self)

        self.current_training_instance = None
        self.current_testing_instance = None

        # continuous (1) or discrete (0) - if discrete, define all possible
        # values
        self.attribute_info = dp.read_names(cons.namesFile)

        train_dta = dp.read_data(cons.trainFile, cons.label_missing_data)

        att_names = [x['att_name'] for x in self.attribute_info]

        # train data
        self.training_data = pd.DataFrame(train_dta, columns=att_names)
        self.n_training_instances = self.training_data.shape[0]
        self.training_dta_access = self.generate_dta_access(self.n_training_instances)

        self.test_training_dta_access = self.generate_test_access(self.n_training_instances)

        max_values = self.training_data.max()
        min_values = self.training_data.min()
        for att in self.attribute_info:
            if att['is_continuous']:
                att['max_value'] = max_values[att['att_name']]
                att['min_value'] = min_values[att['att_name']]
            else:
                att['max_value'] = float(att['att_info'][-1])
                att['min_value'] = float(att['att_info'][0])
    
        
        # test data (if exists)
        if (cons.testFile != None):
            t_dta = dp.read_data(cons.testFile, cons.label_missing_data)
            self.testing_data = pd.DataFrame(t_dta, columns=att_names)
            self.n_testing_instances = self.testing_data.shape[0]
            self.testing_dta_access = self.generate_test_access(self.n_testing_instances)
        else:
            self.testing_data = None
            self.n_testing_instances = 0


        # number of attributes - the last should be the class
        self.num_attributes = len(att_names)-1

        # number of classes
        self.all_classes = self.attribute_info[-1]['att_info']
        self.all_classes = map(str.strip, self.all_classes)
        self.all_classes = map(float, self.all_classes)
        self.num_classes = len(self.all_classes)

        if cons.ruleFile != 'None':
            try:
                self.ruleData = RuleManagement(cons.ruleFile, att_names)
            except:
                self.ruleData = []
        else:
            self.ruleData = []

        pass


    def generate_dta_access(self, dta_length):

        # all_perms = list(it.permutations(xrange(dta_length)))

        perm = range(dta_length)

        dta_index = 0
        
        while 1:
            
            if dta_index == dta_length:
                dta_index = 0
                rand.shuffle(perm)

            
            yield perm[dta_index]

            dta_index += 1


    def generate_test_access(self, dta_length):

        # all_perms = list(it.permutations(xrange(dta_length)))

        perm = range(dta_length)

        dta_index = 0
        
        while 1:
            
            if dta_index == dta_length:
                dta_index = 0

            yield perm[dta_index]

            dta_index += 1




    def get_next_training_instance(self):
        self.current_training_instance = self.training_data.ix[self.training_dta_access.next(), :]
        return self.current_training_instance

    def get_current_training_instance(self):
        return self.current_training_instance

    def get_next_testing_instance(self):
        self.current_testing_instance = self.testing_data.ix[self.testing_dta_access.next(), :]
        return self.current_testing_instance

    def get_current_testing_instance(self):
        return self.current_testing_instance


    def get_next_test_training_instance(self):
        return self.training_data.ix[self.test_training_dta_access.next(), :]

