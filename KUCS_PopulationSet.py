
from KUCS_Constants import *
from KUCS_ClassifiersSet import ClassifiersSet
from KUCS_MatchSet import MatchSet
import random as rand
import copy as cp
import math

import logging

LOGGER = logging.getLogger(__name__)

class PopulationSet(ClassifiersSet):
    def __init__(self, init_pop):

        ClassifiersSet.__init__(self)

        if (init_pop == 3):
            self.expert_population()
        elif (init_pop == 2):
            self.random_population()
        else:
            self.empty_population()


        pass


    def empty_population(self):

        self.set = []


    def random_population(self):
        pass

    def expert_population(self):
        pass

    def form_eval_match_set(self, curr_instance):

        m_set = MatchSet()

        for cl in self.set:
            if cl.match(curr_instance):
                m_set.set.append(cl)

        return m_set


    def form_match_set(self, t_stamp, curr_instance):

        m_set = MatchSet()

        LOGGER.debug('New Match Set')

        for cl in self.set:
            if cl.match(curr_instance):
                LOGGER.debug(' '.join(map(str, cl.condition)))
                m_set.set.append(cl)

        return m_set


    def insert(self, cl):
        for c in self.set:
            if c.equals(cl):
                c.numerosity +=1
                return

        self.set.append(cl)

    #def delete(self):

    #    numerosities_s = sum([cl.numerosity for cl in self.set])
    #    if numerosities_s < cons.N:
    #        return

    #    av_fitness_in_pop = sum([cl.F_micro for cl in self.set])/numerosities_s

    #    vote_sum = 0
    #    for cl in self.set:
    #        cl.p_del = self.deletion_vote(cl, av_fitness_in_pop)
    #        vote_sum += cl.p_del
    #        #vote_sum += self.deletion_vote(cl, av_fitness_in_pop)

    #    choice_point = rand.random() * vote_sum
    #    vote_sum = 0
    #    for cl in self.set:
    #        vote_sum += self.deletion_vote(cl, av_fitness_in_pop)

    #        if (vote_sum > choice_point):
    #            if (cl.numerosity > 1):
    #                cl.numerosity -= 1
    #            else:
    #                self.set.remove(cl)
    #            return


    #def delete(self):

    #    numerosities_s = sum([cl.numerosity for cl in self.set])
    #    if numerosities_s < cons.N:
    #        return

    #    av_fitness_in_pop = sum([cl.F_micro for cl in
    #        self.set])/float(numerosities_s)

    #    vote_sum = 0
    #    for cl in self.set:
    #        cl.p_del = self.del_vote(cl, av_fitness_in_pop)
    #        vote_sum += cl.p_del

    #    for cl in self.set:
    #        cl.p_del /= vote_sum

    #    #self.set.sort(key=lambda x: x.p_del, reverse=True)
    #    self.set.sort(key=lambda x: x.p_del)

    #    if self.set[-1].numerosity > 1:
    #        self.set[-1].numerosity -= 1
    #    else:
    #        LOGGER.info('deleted classifier')
    #        LOGGER.info(self.set[-1].print_cl())
    #        self.set.remove(self.set[-1])

    #    return


    def delete(self):


        numerosities_s = sum([cl.numerosity for cl in self.set])
        #print('entering delete: {} - {}'.format(len(self.set), numerosities_s))
        if numerosities_s < cons.N:
            return


        av_fit_in_pop = sum([cl.F_macro for cl in
            self.set])/float(numerosities_s)

        vote_sum = 0
        for cl in self.set:
            cl.p_del = self.del_vote(cl, av_fit_in_pop)
            vote_sum += cl.p_del

        for cl in self.set:
            cl.p_del /= vote_sum

        #ind = [x for x in range(len(self.set))]
        ind = range(len(self.set))
        rand.shuffle(ind)

        con_size = math.ceil(len(ind) * .25)
        contestants = [self.set[ind.pop()] for n in xrange(int(con_size))]

        contestants.sort(key=lambda x: x.p_del, reverse=True)
        #contestants.sort(key=lambda x: x.p_del)

        for cl in self.set:
            if cl == contestants[0]:
                #print(' '.join(map(str, cl.condition)))
                if cl.numerosity > 1:
                    cl.numerosity -= 1
                else:
                    self.set.remove(cl)
                break

            
        #print('exiting delete: {}'.format(len(self.set)))
        return



    def del_vote(self, cl, av_fit):

        vote = cl.cs_size * cl.numerosity
        if (cl.exp > cons.theta_Del and cl.F_micro < (cons.delta*av_fit)):
            if cl.F_micro != 0:
                vote *= (av_fit/cl.F_micro)

        #return cl.F_micro
        return vote



    def deletion_vote(self, cl, av_fit_in_pop):

        vote = cl.cs_size * cl.numerosity
        if (cl.exp > cons.theta_Del and cl.F_micro < (cons.delta*av_fit_in_pop)):
            if cl.F_micro != 0:
                vote *= (av_fit_in_pop/cl.F_micro)

        return vote
