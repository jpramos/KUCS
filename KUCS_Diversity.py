
# Kuncheva, "Measures of diversity in Classifier ensembles"

N_11 = 0 # both correct
N_10 = 1 # i correct; k wrong
N_01 = 2 # i wrong; k correct
N_00 = 3 # both wrong

def calc_rel_table(cl_i, cl_k):

    if len(cl_i.correctness) != len(cl_k.correctness):
        return None

    rel_tablei_k = [0]*4

    for n in range(len(cl_i.correctness)):
        if cl_i.correctness[n] == cl_k.correctness[n]:
            if cl_i.correctness[n] == 1:
                rel_tablei_k[N_11] += 1
            else:
                rel_tablei_k[N_00] += 1
        else:
            if cl_i.correctness[n] == 1:
                rel_tablei_k[N_10] += 1
            else:
                rel_tablei_k[N_01] += 1


    return rel_tablei_k


def q_statistics(rel_tablei_k):

    if ((rel_tablei_k[N_11]*rel_tablei_k[N_00]) +
            (rel_tablei_k[N_01]*rel_tablei_k[N_10])) != 0:
        qi_k = ((rel_tablei_k[N_11]*rel_tablei_k[N_00]) -
            (rel_tablei_k[N_01]*rel_tablei_k[N_10]))*1.0 / ((rel_tablei_k[N_11]*rel_tablei_k[N_00]) +
            (rel_tablei_k[N_01]*rel_tablei_k[N_10]))*1.0
    else:
        qi_k = 1

    return qi_k

def  avg_q_statistics(pop):

    L = len(pop.set)

    single_qstat = []
    sum_q = 0
    for ind_i in range(L - 1):
        for ind_k in range(ind_i + 1, L):
            rel_table = calc_rel_table(pop.set[ind_i], pop.set[ind_k])
            single_qstat.append((ind_i, ind_k, q_statistics(rel_table),
                rel_table))
            sum_q += single_qstat[-1][2]
            #sum_q += q_statistics(rel_table)


    avg_q = 2.0 / (L * (L - 1)) * sum_q

    return (avg_q, single_qstat)

def double_fault(rel_tablei_k):

    dfi_k = rel_tablei_k[N_00]*1.0 / (rel_tablei_k[N_11] + rel_tablei_k[N_10] +
            rel_tablei_k[N_01] + rel_tablei_k[N_00])*1.0

    return dfi_k

def avg_double_fault(pop):

    L = len(pop.set)

    single_df = []
    sum_df = 0
    for ind_i in range(L -1):
        for ind_k in range(ind_i + 1, L):
            rel_table = calc_rel_table(pop.set[ind_i], pop.set[ind_k])
            single_df.append((ind_i, ind_k, double_fault(rel_table), rel_table))
            sum_df += single_df[-1][2]
            #sum_df += double_fault(rel_table)


    avg_df = 2.0 / (L * (L - 1)) * sum_df

    return (avg_df, single_df)


def disagreement(rel_tablei_k):

    disi_k = (rel_tablei_k[N_01] * rel_tablei_k[N_10]) * 1.0 / (rel_tablei_k[N_00] + rel_tablei_k[N_01] + rel_tablei_k[N_10] +
            rel_tablei_k[N_11])*1.0

    return disi_k


def avg_disagreement(pop):

    L = len(pop.set)

    single_dis = []
    sum_dis = 0
    for ind_i in range(L-1):
        for ind_k in range(ind_i + 1, L):
            rel_table = calc_rel_table(pop.set[ind_i], pop.set[ind_k])
            single_dis.append((ind_i, ind_k, disagreement(rel_table), rel_table))
            sum_dis += single_dis[-1][2]

    avg_dis = 2.0 / (L * (L - 1)) * sum_dis

    return (avg_dis, single_dis)

