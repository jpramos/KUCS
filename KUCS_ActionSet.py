

from KUCS_Constants import *
from KUCS_ClassifiersSet import ClassifiersSet
from KUCS_Classifier import Classifier
import random as rand

class ActionSet(ClassifiersSet):
    def __init__(self, parent_mset):
        ClassifiersSet.__init__(self)

        self.m_set = parent_mset
        
        pass


    def update_set(self):

        set_size = sum([cl.numerosity for cl in self.set])
        for cl in self.set:

            cl.corr_set_counter += 1
            #cl.cs_size = set_size / float(cl.corr_set_counter)
            cl.cs_size += (set_size - cl.cs_size) / float(cl.corr_set_counter)

        # if do action_set_subsumption
        if (cons.do_actionset_subsumption):
            self.action_set_subsumption(pop)

        return None


    def update_fitness(self):

        acc_sum = 0

        k = [0] * len(self.set)
        for (ind, cl) in enumerate(self.set):

            if cl.eps <= cons.eps_0:
                k[ind] = 1
            else:
                k[ind] = cons.alpha * (cl.eps / cons.eps_0) ** (-cons.nu)

            acc_sum += k[ind] * cl.numerosity

        for (ind, cl) in enumerate(self.set):
            cl.fitness += cons.beta * (k[ind] * cl.numerosity / acc_sum - cl.fitness)


    def action_set_subsumption(self, pop):

        cl = None
        for c in self.set:
            if (c.could_subsume()):
                if not cl or c.count_dontcares() > cl.count_dontcares() or (c.count_dontcares() == cl.count_dontcares() and rand.random() < .5):
                    cl = c

        if cl:
            for c in self.set:
                if (cl.is_more_general(c)):
                    cl.numerosity += c.numerosity
                    self.set.remove(c)
                    pop.set.remove(c)


    def covering(self, t_stamp, pop, curr_instance, action):

        cl_new = Classifier()
        cl_new.covering(t_stamp, curr_instance, action)

        self.set.append(cl_new)
        self.m_set.set.append(cl_new)
        #pop.set.append(cl_new)
        pop.insert(cl_new)

        pop.delete()


    def is_experienced(self):

        whole_exp = 0
        for cl in self.set:
            whole_exp += cl.exp

        avg_exp = whole_exp / float(len(self.set))

        if avg_exp > cons.N_sp:
            return True

        return False


    def specify(self, pop, inst, curr_time):

        if self.get_error_avg() > (2.0 * pop.get_error_avg()) and self.is_experienced():
        #if self.get_acc_avg() <= (pop.get_acc_avg()/2.0) and self.is_experienced():

            s_numerosities = sum([cl.numerosity for cl in self.set])

            whole_acc = 0
            for cl in self.set:
                whole_acc += cl.acc

            r_choice = rand.random() * whole_acc
            whole_acc = 0

            for cl in self.set:
                whole_acc += cl.acc
                if whole_acc > r_choice:
                    new_cl = cl.copy(curr_time)
                    #new_cl.cs_size = s_numerosities
                    new_cl.specify(inst)

                    pop.insert(new_cl)
                    pop.delete()
                    return

            
        return


    #def specify(self, pop, inst, curr_time):

    #    if self.get_error_avg() > (2.0 * pop.get_error_avg()) and self.is_experienced():

    #        s_numerosities = sum([cl.numerosity for cl in self.set])

    #        whole_err = 0
    #        for cl in self.set:
    #            whole_err += (1 - cl.acc)

    #        r_choice = rand.random() * whole_err
    #        whole_err = 0

    #        for cl in self.set:
    #            whole_err += (1 - cl.acc)
    #            if whole_err > r_choice:
    #                new_cl = cl.copy(curr_time)
    #                #new_cl.cs_size = s_numerosities
    #                new_cl.specify(inst)

    #                pop.insert(new_cl)
    #                pop.delete()
    #                return

    #        
    #    return


