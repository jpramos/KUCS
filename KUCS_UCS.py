
from KUCS_PopulationSet import PopulationSet
from KUCS_MatchSet import MatchSet
from KUCS_ActionSet import ActionSet
from KUCS_Constants import *
from KUCS_GA import GA
import random as rand
import pickle as pck
import KUCS_Pruning as pruning

import logging

PREV_VAL = 0
LOGGER = logging.getLogger(__name__)

class UCS:
    def __init__(self, env=None, rp=None):
        
        # pass the environment reference
        self.env = env

        # pass the reinforcement program reference
        self.rp = rp


        # init time-step
        self.time_t = 1


        # init population
        # either start
        # (1) an empty population
        # (2) at random
        # (3) from previous set of knowledge rules
        init_pop = 1
        self.population = PopulationSet(init_pop)

    def run(self):

        ga = GA()

        while (self.time_t < cons.max_iter):

            #print('iteration time: {}'.format(self.time_t))

            # read an instance
            instance = self.env.get_next_training_instance()

            LOGGER.info('iteration time: %s\tinstance %s', self.time_t, ' '.join(map(str, instance)))
            # print('time: {}, instance: {}'.format(self.time_t, instance))

            #LOGGER.debug('---- Population ----')
            #self.population.print_set()
            #LOGGER.debug('---- End Population ----')

            # form a match set
            match_set = self.population.form_match_set(self.time_t, instance)

            #LOGGER.debug('--- Match ---')
            #match_set.print_set()
            #LOGGER.debug('--- End Match ---')


            action = instance[-1]

            # generate action set out of match_set according to selected action
            action_set = match_set.form_action_set(action)

            #LOGGER.debug('--- Action ---')
            #action_set.print_set()
            #LOGGER.debug('--- End Action ---')

            if action_set.isempty():
                #print("empty action set")
                LOGGER.debug('empty action set')
                action_set.covering(self.time_t, self.population, instance,
                        instance[-1])



            #P = rho
            # update set M using P
            match_set.update_set(action)
            action_set.update_set()
            action_set.specify(self.population, instance, self.time_t)

            # Run GA in set A
            ga.run_ga(self.time_t, action_set, instance, self.population)

            #LOGGER.info('before')
            #self.evaluate()
            #self.population.set.sort(key=lambda x: x.birthday)
            #LOGGER.info('after')
            #self.evaluate()
            #if self.time_t % cons.env.n_training_instances == 0:
            ##    print("----eval--- {}".format(self.time_t))
            #    #print('training----')
            #    self.evaluate_train()
            ##    print('testing--- {}'.format(sum([1 for cl in
            ##        self.population.set ])))
            #    self.evaluate()
            ##    #LOGGER.debug('------')
            ##    #self.population.set.sort(key=lambda x: x.birthday)
            ##    #self.population.print_set()
            #    self.population.write_set_to_file('out/pop_{}.csv'.format(self.time_t))
            #    self.population.write_conditions_to_file('out/cond_{}.txt'.format(self.time_t))
            ##    #LOGGER.debug("\n".join(map(str, [cl.condition for cl in
            ##    #    self.population.set])))
             
            self.time_t += 1

        #self.evaluate_train()
        self.evaluate()
        print(sum([cl.action for cl in self.population.set]))

        #pruning.population_pruning(self.population, cons.env.training_data,
        #        cons.env.testing_data)
        pruning.p_pr(self.population, cons.env.training_data,
                cons.env.testing_data)

        self.population.set.sort(key=lambda x: x.birthday)
        self.population.print_set()
        LOGGER.debug("\n".join(map(str, [(cl.condition, cl.action, cl.exp,
            cl.birthday, cl.acc, cl.F_macro, cl.numerosity) for cl in
            self.population.set])))

        pck.dump(self.population, open('pop.pickle', 'wb'))
        pck.dump(self.env.training_data, open('train_dta.pickle', 'wb'))
        pck.dump(self.env.testing_data, open('test_dta.pickle', 'wb'))

        return None

    def evaluate_train(self):

        num_tests = cons.env.n_training_instances

        y_true = []
        y_score = []
        covered_count = 0
        for x in range(num_tests):

            inst = cons.env.get_next_test_training_instance()

            m_set = self.population.form_eval_match_set(inst)
            (action, is_covered) = m_set.cast_votes()

            if is_covered:
                covered_count += 1

            LOGGER.debug(inst)
            LOGGER.debug('\n'.join(map(str, [(cl.condition, cl.action, cl.exp,
                cl.F_macro, cl.F_macro*(cl.exp / float(cons.theta_Del)), cl.corr_set_counter) for cl in m_set.set])))
            LOGGER.debug("true: {}; got: {}".format(inst[-1], action))

            y_true.append(inst[-1])
            y_score.append(float(action))


        tp = 0
        tn = 0
        fp = 0
        fn = 0
        for (t, p) in zip(y_true, y_score):
            if t:
                if t == p:
                    tp += 1
                else:
                    fn += 1
            else:
                if t == p:
                    tn += 1
                else:
                    fp += 1

        sens = tp / float(tp + fn)
        spec = tn / float(tn + fp)
        acc = (tp + tn) / float(tp + fn + tn + fp)
        gmean = (sens * spec) ** (1/2.0)

        #with open("eval.txt", "a") as fd:
            #fd.write("{}\t{}\t{}\n".format(acc, sens, spec))
        LOGGER.info('acc: %s, sens: %s, spec, %s', acc, sens, spec)

        print("sens: {} ; spec: {}, gmean: {} \t {} / {}".format(sens, spec,
            gmean, covered_count, num_tests))

    def evaluate(self):

        num_tests = cons.env.n_testing_instances

        y_true = []
        y_score = []
        covered_count = 0
        for x in range(num_tests):

            inst = cons.env.get_next_testing_instance()

            m_set = self.population.form_eval_match_set(inst)
            (action, is_covered) = m_set.cast_votes()

            if is_covered:
                covered_count += 1

            LOGGER.debug(' '.join(map(str, inst)))
            LOGGER.debug('\n'.join(map(str, [(cl.condition, cl.action, cl.exp,
                cl.F_macro, cl.F_macro*(cl.exp / float(cons.theta_Del)), cl.corr_set_counter) for cl in m_set.set])))
            LOGGER.debug("true: {}; got: {}".format(inst[-1], action))
            y_true.append(inst[-1])
            y_score.append(float(action))


        tp = 0
        tn = 0
        fp = 0
        fn = 0
        for (t, p) in zip(y_true, y_score):
            if t:
                if t == p:
                    tp += 1
                else:
                    fn += 1
            else:
                if t == p:
                    tn += 1
                else:
                    fp += 1

        sens = tp / float(tp + fn)
        spec = tn / float(tn + fp)
        acc = (tp + tn) / float(tp + fn + tn + fp)
        gmean = (sens * spec) ** (1/2.0)

        global PREV_VAL
        PREV_VAL = sens

        with open("eval_wout_pruning.txt", "a") as fd:
            fd.write("{}\t{}\t{}\t{}\n".format(acc, sens, spec, gmean))

        with open('pop_wout_pruning.txt', 'a') as fd:
            fd.write('{}\n'.format(len(self.population.set)))

        #with open("eval{}_{}.txt".format(cons.N, cons.p_WD), "a") as fd:
        #    fd.write("{}\t{}\t{}\t{}\n".format(acc, sens, spec, gmean))
        LOGGER.info('acc: %s, sens: %s, spec, %s', acc, sens, spec)

        print("sens: {} ; spec: {}, gmean: {} \t {} / {}".format(sens, spec,
            gmean, covered_count, num_tests))

