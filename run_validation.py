
import oversampling as over_s
import os
import shutil as sh
import re

IN_PATH = 'input_data/'
OUT_PATH = 'output_data/'
DATASET = 'multiplexer_11'
#DATASET = 'infarctionA'
K_FOLDS = 10
N_TIMES = 5
CONFIG_FILE = 'KUCS_CONFIG.txt'
N = 900
PD = 0.6

def eval_ucs(res_file):

    with open(res_file, 'r') as fd:

        line = fd.readlines()[-1].split('\t')
        SN = float(line[1])
        SP = float(line[2])
        G_MEAN = float(line[3])


    return [G_MEAN, SN, SP]


def eval_c50(res_file):

    TP = 0
    TN = 0
    FP = 0
    FN = 0

    with open(res_file, 'r') as fd:
        for line in fd.readlines():
            l = line.split(',')
            true_v = float(l[0])
            predict_v = float(l[1])

            if true_v == predict_v:
                if true_v > 0:
                    TP += 1
                else:
                    TN += 1
            else:
                if predict_v > 0 and true_v == 0:
                    FP += 1
                else:
                    FN += 1

    SN = TP*1.0 / (TP + FN)*1.0
    SP = TN*1.0 / (TN + FP)*1.0

    G_MEAN = (SN * SP) ** (1/2.0)

    return [G_MEAN, SN, SP]


ucs_wout_pruning_perf = []
ucs_with_pruning_perf = []

for n in xrange(N_TIMES):
    # create cross validation datasets with oversampling
    over_s.create_cross_val_sets(IN_PATH + DATASET + '.data', K_FOLDS)

    #for each k-fold
    for it in xrange(K_FOLDS):

        # run ucs
        with open(CONFIG_FILE, 'r+') as content_file:
            content = content_file.read()
            # copy train data
            train_f = DATASET + '_' + str(it) + '_train.txt'
            sh.copy(OUT_PATH + train_f, '.')
            new_content = re.sub('(?<=trainFile\=)\w+\.txt', train_f, content)

            # copy test data
            test_f = DATASET + '_' + str(it) + '_test.txt'
            sh.copy(OUT_PATH + test_f, '.')
            new_content = re.sub('(?<=testFile\=)\w+(\.txt)*', test_f, new_content)

            content_file.seek(0)
            content_file.write(new_content)

        os.system('python KUCS_Run.py')

        ## evaluate UCS results without pruning
        ucs_wout_pruning_perf.append(eval_ucs('eval_wout_pruning.txt'))
        ucs_with_pruning_perf.append(eval_ucs('eval_with_pruning.txt'))

        # evaluate UCS results with pruning




    #end cycle
with open('ucs_wout_pruning_perf.txt', 'w') as fd:
	for ln in ucs_wout_pruning_perf:
		fd.write('\t'.join(map(str, ln)))
		fd.write('\n')

with open('ucs_with_pruning_perf.txt', 'w') as fd:
	for ln in ucs_with_pruning_perf:
		fd.write('\t'.join(map(str, ln)))
		fd.write('\n')
# compare methods




