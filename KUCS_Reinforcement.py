
from KUCS_OfflineEnvironment import OfflineEnvironment
from KUCS_Constants import *

class Reinforcement:
    def __init__(self, env):

        self.env = env


    def get_reward(self, action, pa):

        curr_instance = self.env.get_current_training_instance()

        if curr_instance[-1] == float(action):
            # reward = sum(filter(None, pa))
            reward = 1000
        else:
            reward = 0

        return reward


    def is_end_of_problem(self):
        return True

