
from KUCS_Constants import *
import random as rand
import copy as cp
import math

import logging

LOGGER = logging.getLogger(__name__)

class GA:

    def __init__(self):
        pass

    def run_ga(self, curr_time, set, inst, pop):

        tmp_sum = sum([cl.ts * cl.numerosity for cl in set.set])
        sum_numerosity = sum([cl.numerosity for cl in set.set])
        if sum_numerosity == 0:
            return

        #if len(set.set) < 2:
            #return

        if (curr_time - (tmp_sum/sum_numerosity)) > cons.theta_GA:
 
            LOGGER.debug('Entering GA...')
            for cl in set.set:
                cl.ts = curr_time
            #parent_1 = self.tournament_sel(set)
            #parent_2 = self.tournament_sel(set)
            (parent_1, parent_2) = self.tourn_sel(set, 2)
            #parent_1 = self.select_offspring(set)
            #parent_2 = self.select_offspring(set)
            child_1 = cp.deepcopy(parent_1)
            child_2 = cp.deepcopy(parent_2)
            child_1.birthday = curr_time
            child_2.birthday = curr_time
            child_1.numerosity = 1
            child_2.numerosity = 1
            child_1.exp = 0
            child_2.exp = 0
            child_1.acc = 0
            child_2.acc = 0
            #child_1.cs_size = (parent_1.cs_size + parent_2.cs_size) / 2.0
            #child_2.cs_size = (parent_1.cs_size + parent_2.cs_size) / 2.0
            #child_1.acc = (parent_1.acc + parent_2.acc) / 2.0
            #child_2.acc = (parent_1.acc + parent_2.acc) / 2.0
            ##child_1.F_micro = 0
            ##child_1.F_macro = 0
            ##child_2.F_micro = 0
            ##child_2.F_macro = 0
            #child_1.F_micro = (parent_1.F_micro + parent_2.F_micro)/2.0*.1
            #child_1.F_macro = child_1.F_micro
            #child_2.F_micro = (parent_1.F_micro + parent_2.F_micro)/2.0*.1
            #child_2.F_macro = child_2.F_micro
            child_1.corr_set_counter = 0
            child_2.corr_set_counter = 0
            child_1.corr_class = 0
            child_2.corr_class = 0
            child_1.acc_i = [1]*cons.env.num_classes
            child_2.acc_i = [1]*cons.env.num_classes
            child_1.exp_i = [1]*cons.env.num_classes
            child_2.exp_i = [1]*cons.env.num_classes
            child_1.corr_class_i = [1]*cons.env.num_classes
            child_2.corr_class_i = [1]*cons.env.num_classes
            child_1.cs_acc = 0
            child_2.cs_acc = 0
            child_1.p_del = 0
            child_2.p_del = 0

            if (rand.random() < cons.chi):
                child_1, child_2 = self.apply_crossover(child_1, child_2)

                child_1.cs_size = (parent_1.cs_size + parent_2.cs_size) / 2.0
                child_2.cs_size = (parent_1.cs_size + parent_2.cs_size) / 2.0
                #child_1.acc = 1
                #child_2.acc = 1
                #child_1.acc = (parent_1.acc + parent_2.acc) / 2.0
                #child_2.acc = (parent_1.acc + parent_2.acc) / 2.0
                #child_1.F_micro = 0
                #child_1.F_macro = 0
                #child_2.F_micro = 0
                #child_2.F_macro = 0
                child_1.F_micro = (parent_1.F_micro + parent_2.F_micro)/2.0
                #child_1.F_macro = child_1.F_micro
                child_2.F_micro = (parent_1.F_micro + parent_2.F_micro)/2.0
                #child_2.F_macro = child_2.F_micro

            child_1.F_micro *= .1
            child_2.F_micro *= .1
            child_1.F_macro = child_1.F_micro
            child_2.F_macro = child_2.F_micro

            for child in [child_1, child_2]:
                child = self.apply_mutation(child, inst)

                if (cons.do_GA_subsumption):
                    if parent_1.does_subsume(child):
                        parent_1.numerosity +=1
                    elif parent_2.does_subsume(child):
                        parent_2.numerosity += 1
                    else:
                        pop.insert(child)
                else:
                    pop.insert(child)

                pop.delete()


    def select_offspring(self, set):

        fitness_sum = sum([cl.F_macro if (cl.exp >= cons.theta_Del) else (cl.F_macro/cons.theta_Del) for cl in set.set])

        choice_point = rand.random() * fitness_sum
        fitness_sum = 0

        for cl in set.set:
            cl_f = cl.F_macro
            if cl.exp < cons.theta_Del:
                cl_f /= cons.theta_Del
            fitness_sum += cl_f
            if fitness_sum > choice_point:
                return cl

    def tournament_sel(self, set, n_sels):

        set_copy = cp.deepcopy(set)
        rand.shuffle(set_copy.set)

        con_size = math.ceil(len(set.set) * .5)
        contestants = [set_copy.set.pop() for n in xrange(int(con_size))]

        contestants.sort(key=lambda x: x.F_macro, reverse=True)

        return [contestants[sels%len(contestants)] for sels in range(n_sels)]

    def tourn_sel(self, set, n_sels):

        ret = []
        set_copy = cp.deepcopy(set)

        if len(set_copy.set) < n_sels:
            set_copy.set.extend([rand.choice(set_copy.set) for _ in
                range(n_sels - len(set_copy.set))])

        for sels in range(n_sels):

            rand.shuffle(set_copy.set)

            con_size = math.ceil(len(set_copy.set) * .5)
            contestants = [set_copy.set[n] for n in xrange(int(con_size))]

            contestants.sort(key=lambda x: x.F_macro, reverse=True)

            ret.append(contestants[0])

            set_copy.set.remove(contestants[0])

        return ret



    def apply_crossover(self, cl1, cl2):

        perm = range(len(cl1.condition))
        rand.shuffle(perm)
        x = perm.pop()
        y = perm.pop()
        #x = int(math.floor(rand.random() * (len(cl1.condition) + 1)))
        #y = int(math.floor(rand.random() * (len(cl1.condition) + 1)))

        if (x > y):
            tmp = y
            y = x
            x = tmp

        for ind in range(x, y+1):
            tmp = cl2.condition[ind]
            cl2.condition[ind] = cl1.condition[ind]
            cl1.condition[ind] = tmp

        #for (g1, g2) in zip(cl1.condition[x:y], cl2.condition[x:y]):
        #    tmp = g2
        #    g2 = g1
        #    g1 = tmp

        return cl1, cl2


    def apply_mutation(self, cl, inst):

        for (ind, ge) in enumerate(cl.condition):
            att_info = cons.env.attribute_info[ind]
            if rand.random() < cons.mu:
                if ge == cons.dont_care_sym:
                    if att_info['is_continuous']:
                        cl.condition[ind] = cl.continuous_att(ind, inst[ind])
                    else:
                        cl.condition[ind] = inst[ind]
                else:
                    if cons.env.ruleData:
                        bias = cons.env.ruleData.check_rules_activation(ind,
                                inst[ind])
                    else:
                        bias = 0
                    mutate_p = .5 + bias
                    if rand.random() < mutate_p:
                        if att_info['is_continuous']:
                            cl.condition[ind] = self.mutate_continuous(ge, att_info)
                        else:
                            cl.condition[ind] = self.mutate_discrete(ge, att_info)
                    else:
                        cl.condition[ind] = cons.dont_care_sym

        if rand.random() < cons.mu:
            other_options = [x for x in cons.env.all_classes if x != cl.action]
            cl.action = rand.choice(other_options)
            #cl.action = rand.choice(cons.env.all_classes)

        return cl

    def mutate_discrete(self, gene, att_info):

        possible_genes = [g for g in att_info['att_info'] if float(g) != gene]
        gene = float(rand.choice(possible_genes))

        return gene


    def mutate_continuous(self, gene, att_info):

        #att_range = att_info['max_value'] - att_info['min_value']
        phen_range = gene[1] - gene[0]
        mutate_range = rand.random()*.5*phen_range

        if rand.random() < .5:
            if rand.random() < .5:
                gene[0] += mutate_range
            else:
                gene[0] -= mutate_range
        else:
            if rand.random() < .5:
                gene[1] += mutate_range
            else:
                gene[1] -= mutate_range

        return gene
