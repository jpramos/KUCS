

from KUCS_Constants import *
import pandas as pd
import random as rand
import copy as cp

import logging

LOGGER = logging.getLogger(__name__)

class Classifier:
    def __init__(self):

        # the condition specifies the input states (sensory situations) in
        # which the classifier can be applied (matches)
        self.condition = []

        # the action specifies the action (possibly a classification) that the
        # classifier proposes
        self.action = None

        # the fitness denotes the classifier's fitness
        self.fitness = None

        # the experience exp counts the number of times since its creation that
        # the classifier has belonged to a match set
        self.exp = None

        # the time stamp denotes the time-step of the last occurence of a GA in
        # an action set to which this classifier belonged
        self.ts = None

        # the correct set size estimates the average of the correct sets this
        # classifier has belonged to
        self.cs_size = None

        # the numerosity reflects the number of micro-classifiers this
        # classifier represents
        self.numerosity = None

        # number of correct classifications
        self.corr_class = None

        # accuracy of the classifier
        self.acc = None

        # micro classifier fitness
        self.F_micro = None

        # macro classifier fitness
        self.F_macro = None

        # correct set counter
        self.corr_set_counter = None

        # experience on a class
        self.exp_i = None

        # accuracy on a class
        self.acc_i = None

        # correct classification on a class
        self.corr_class_i = None

        # class_sensitive acc
        self.cs_acc = None

        # classifier birthday
        self.birthday = None

        # probability of deletion
        self.p_del = None

        # dont cares counter
        self.dontcare_counter = None

        # save classification for pruning evaluation
        self.correctness = []


    def covering(self, t_stamp, instance, new_action):

        for (i, att) in enumerate(cons.env.attribute_info[:-1]):
            if cons.env.ruleData:
                bias = cons.env.ruleData.check_rules_activation(i, instance[i])
            else:
                bias = 0
            cover_p = cons.p_WD - bias
            if (rand.random() < cover_p):
                self.condition.append(cons.dont_care_sym)
            else:
                if att['is_continuous']:
                    x = self.continuous_att(i, instance[i]) # rectangular stuff
                    self.condition.append(x)
                else:
                    self.condition.append(instance[i])


        self.action = new_action
        self.fitness = cons.f_I
        self.exp = 0
        self.ts = t_stamp
        self.cs_size = 1
        self.numerosity = 1
        self.corr_class = 0
        self.acc = 0
        self.F_micro = 0
        self.F_macro = 0
        self.corr_set_counter = 0
        self.acc_i = [1] * cons.env.num_classes
        self.exp_i = [1] * cons.env.num_classes
        self.corr_class_i = [1]*cons.env.num_classes
        self.cs_acc = 0
        self.birthday = t_stamp
        self.p_del = 0
        self.dontcare_counter = 0

        LOGGER.debug('New Classifier at time {} with instance {}'.format(t_stamp, ' '.join(map(str, instance))))
        LOGGER.debug(self.print_cl(True))
        LOGGER.debug(self.condition)



    def copy(self, curr_time):

        cl_copy = cp.deepcopy(self)
        #cl_copy.birthday = curr_time
        cl_copy.numerosity = 1
        #cl_copy.exp = 0
        #cl_copy.cs_size = 0
        #cl_copy.acc = 0
        #cl_copy.F_micro = 0
        #cl_copy.F_macro = 0
        cl_copy.corr_set_counter = 0
        #cl_copy.corr_class = 0
        #cl_copy.acc_i = [1]*cons.env.num_classes
        #cl_copy.exp_i = [1]*cons.env.num_classes
        #cl_copy.corr_class_i = [1]*cons.env.num_classes
        #cl_copy.cs_acc = 0
        #cl_copy.p_del = 0

        return cl_copy

    def continuous_att(self, att_ref, val):
        att_info = cons.env.attribute_info[att_ref]

        att_range = att_info['max_value'] - att_info['min_value']
        range_radius = rand.randint(25, 75)*.01*att_range / 2.0
        # range_radius = att_range /2.0
        low = val - range_radius
        high = val + range_radius
        cont_val = [low, high]

        return cont_val


    def match(self, curr_instance):

        for ((i, cond),attr) in zip(enumerate(self.condition),
                curr_instance[:-1]):
            #print('gene: {}, instance_val: {}'.format(cond, attr))
            if cond == cons.dont_care_sym or pd.isnull(attr):
                continue

            if cons.env.attribute_info[i]['is_continuous']:
                if cond[0] <= curr_instance[i] <= cond[1]:
                    continue
                else:
                    return False

            else:
                if cond != curr_instance[i]:
                    return False

        return True
            

    def equals(self, other_cl):

        for (ind, ge) in enumerate(self.condition):

            if ge == cons.dont_care_sym and other_cl.condition[ind] != cons.dont_care_sym:
                return False

            if other_cl.condition[ind] == cons.dont_care_sym and ge != cons.dont_care_sym:
                return False

            if other_cl.condition[ind] == cons.dont_care_sym and ge == cons.dont_care_sym:
                continue

            att_info = cons.env.attribute_info[ind]

            if att_info['is_continuous']:
                if ge[0] != other_cl.condition[ind][0] or ge[1] != other_cl.condition[ind][1]:
                    return False
            else:
                if ge != other_cl.condition[ind]:
                    return False

        if self.action != other_cl.action:
            return False


        return True


    def count_dontcares(self):

        return sum([1 for loc in self.condition if loc == cons.dont_care_sym])
            

    def could_subsume(self):
        if self.exp > cons.theta_Sub:
            if self.acc > cons.acc_0:
                return True
            #if self.eps < cons.eps_0:
                #return True
            
        return False

    def is_more_general(self, other_cl):

        if self.count_dontcares() <= other_cl.count_dontcares():
            return False

        for ind in xrange(len(self.condition)):
            if self.condition[ind] != cons.dont_care_sym:

                att_info = cons.env.attribute_info[ind]

                if att_info['is_continuous']:
                    if self.condition[ind][0] < other_cl.condition[ind][0] or self.condition[ind][1] > other_cl.condition[ind][1]:
                        return False
                else:
                    if self.condition[ind] != other_cl.condition[ind]:
                        return False


        return True

    def does_subsume(self, other_cl):
        if self.action == other_cl.action:
            if self.could_subsume():
                if self.is_more_general(other_cl):
                    return True

        return False


    def specify(self, inst):

        for ind in range(len(self.condition)):

            if self.condition[ind] == cons.dont_care_sym:
                if rand.random() < cons.P_sp:
                    att_info = cons.env.attribute_info[ind]
                    if att_info['is_continuous']:
                        self.condition[ind] = self.continuous_att(ind, inst[ind])
                    else:
                        self.condition[ind] = inst[ind]

        return None


    def print_cl(self, print_header=True):

        #for (ind,loc) in enumerate(self.condition):
            #print("{} - {}".format(cons.env.attribute_info[ind]['att_name'], loc))
        atrs = vars(self)
        if print_header:
            header = self.print_header() + '\n'
        else:
            header = ''

        #print('\t'.join("%s" % item[1] for item in atrs.items() if item[0] is
            #not 'condition'))
        self.dontcare_counter = self.count_dontcares()
        info = '\t'.join("%s" % item[1] for item in sorted(atrs.items(),
            key=lambda tup: tup[0]) if item[0] is
            not 'condition')

        #print('condition: {}\t action: {}'.format(' '.join(map(str,
            #self.condition)), self.action))
        #print("action - {}".format(self.action))

        return header + info

    def print_header(self):

        atrs = vars(self)
        #print('\t'.join("%s" % item[0] for item in atrs.items() if item[0]
                #is not 'condition'))
        str = '\t'.join("%s" % item[0] for item in sorted(atrs.items(),
            key=lambda tup: tup[0]) if item[0]
                is not 'condition')

        return str
