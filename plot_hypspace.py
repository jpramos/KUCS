
import pickle as pck
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import os
from matplotlib.colors import ListedColormap

EPS = np.finfo(float).eps

DONT_CARE = '#'

plt.ion()

def plot_data(X, y, ax, x_min, x_max, y_min, y_max):


    xx, yy = np.meshgrid(np.arange(x_min, x_max, .02), np.arange(y_min, y_max,
        .02))

    cm = plt.cm.RdBu
    cm_bright = ListedColormap(['#FF0000', '#0000FF'])
    ax = plt.subplot(1, 1, 1)
    ax.scatter(X[:,0], X[:,1], c=y, cmap=cm_bright)
    ax.set_xlim(xx.min(), xx.max())
    ax.set_ylim(yy.min(), yy.max())

    return None

def plot_rules(pop, ax, x_min, x_max, y_min, y_max):

    pos_count = 0
    neg_count = 0

    for ind in pop.set:
        rule = ind.condition
        x_bound = rule[0]
        y_bound = rule[1]

        if x_bound == DONT_CARE:
            x_bound = [x_min, x_max]

        if y_bound == DONT_CARE:
            y_bound = [y_min, y_max]


        x = min(x_bound)
        y = min(y_bound)

        width = abs(x_bound[1] - x_bound[0])
        height = abs(y_bound[1] - y_bound[0])

        if ind.action == 0:
            c = '#FF0000'
            neg_count += 1
        else:
            c = '#0000FF'
            pos_count += 1

        pr = patches.Rectangle((x, y), width, height, alpha=.1,
                color=c)
        ax.add_patch(pr)


    ax.set_title('pos: {} / neg: {} / total: {}'.format(pos_count, neg_count,
        pos_count + neg_count))


    return None

        


pop = pck.load(file('pop.pickle'))
pop_pruned = pck.load(file('pop_pruned.pickle'))

train_dta = pck.load(file('train_dta.pickle'))
test_dta = pck.load(file('test_dta.pickle'))

#X = train_dta.values[:,0:2]
#y = train_dta.values[:,2]

train_X = train_dta.values[:,0:2]
train_y = train_dta.values[:,2]

train_x_min, train_x_max = train_X[:,0].min()-.5, train_X[:,0].max()+.5
train_y_min, train_y_max = train_X[:,1].min()-.5, train_X[:,1].max()+.5


test_X = test_dta.values[:,0:2]
test_y = test_dta.values[:,2]

test_x_min, test_x_max = test_X[:,0].min()-.5, test_X[:,0].max()+.5
test_y_min, test_y_max = test_X[:,1].min()-.5, test_X[:,1].max()+.5

#x_min, x_max = X[:,0].min() - .5, X[:,0].max()+.5
#y_min, y_max = X[:,1].min() - .5, X[:,1].max()+.5
#
train_figure = plt.figure(figsize=(27,9))
train_ax = plt.subplot(1, 1, 1)

plot_data(test_X, test_y, train_ax, test_x_min, test_x_max, test_y_min, test_y_max)

plot_rules(pop, train_ax, test_x_min, test_x_max, test_y_min, test_y_max)
#plot_rules(pop_pruned, train_ax, train_x_min, train_x_max, train_y_min, train_y_max)
#xx, yy = np.meshgrid(np.arange(x_min, x_max, .02), np.arange(y_min, y_max,
#    .02))
#
#cm = plt.cm.RdBu
#cm_bright = ListedColormap(['#FF0000', '#0000FF'])
#ax.scatter(X[:,0], X[:,1], c=y, cmap=cm_bright)
#ax.set_xlim(xx.min(), xx.max())
#ax.set_ylim(yy.min(), yy.max())
#ax.set_xticks(())
#ax.set_yticks(())


#plt.show()

path, dirs, files = os.walk("./out_images/").next()
file_count = len(files)
plt.savefig('./out_images/'+str(file_count+1)+'_pop')
plt.close()


pruned_figure = plt.figure(figsize=(27,9))
pruned_ax = plt.subplot(1, 1, 1)


plot_data(test_X, test_y, pruned_ax, test_x_min, test_x_max, test_y_min, test_y_max)

plot_rules(pop_pruned, pruned_ax, test_x_min, test_x_max, test_y_min, test_y_max)
#
#plt.show()
#path, dirs, files = os.walk("./out_images/").next()
#file_count = len(files)
plt.savefig('./out_images/'+str(file_count+1)+'_pop_pruned')
plt.close()


