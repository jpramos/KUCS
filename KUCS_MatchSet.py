

from KUCS_Constants import *
from KUCS_ClassifiersSet import ClassifiersSet
from KUCS_ActionSet import ActionSet
from KUCS_Classifier import Classifier
import random as rand

import logging

LOGGER = logging.getLogger(__name__)

class MatchSet(ClassifiersSet):
    def __init__(self):
        ClassifiersSet.__init__(self)
        pass


    def count_actions(self):
        return len(set([cl.action for cl in self.set]))


    def covering(self, t_stamp, curr_instance):

        existing_actions = [cl.action for cl in self.set]
        missing_actions = [x for x in cons.env.all_classes if x not in existing_actions]
        new_action = rand.choice(missing_actions)

        cl_new = Classifier()
        cl_new.covering(t_stamp, curr_instance, new_action)

        return cl_new


    def generate_prediction_array(self):
        pa = [None] * cons.env.num_classes #len(self.set)
        fsa = [0] * cons.env.num_classes

        for cl in self.set:
            curr_action = cons.env.all_classes.index(cl.action)
            if pa[curr_action] == None:
                pa[curr_action] = cl.prediction * cl.fitness
            else:
                pa[curr_action] += cl.prediction * cl.fitness

            fsa[curr_action] += cl.fitness

        for action in xrange(cons.env.num_classes):
            if (fsa[action] != 0):
                pa[action] = pa[action] / fsa[action]


        return pa


    def cast_votes(self):

        voting_bin = [0] * cons.env.num_classes
        voting_count = [0] * cons.env.num_classes

        for cl in self.set:

            curr_action = cons.env.all_classes.index(cl.action)
            
            cl_vote = cl.F_macro
            #cl_vote *= cl.corr_set_counter
            if (cl.exp < cons.theta_Del):
                cl_vote *= (cl.exp / float(cons.theta_Del))

            #LOGGER.debug("action {} \t {}".format(cl.action, cl_vote))
            voting_bin[curr_action] += cl_vote 
            voting_count[curr_action] += cl.numerosity # 03/03/2016 jpramos 
            #voting_count[curr_action] += 1

            #voting_bin[curr_action] += (cl_vote / all_weights)

        #voting_bin[0] = 0
        #if voting_bin[1] < 1e-10:
        #    voting_bin[1] = 0
        voting_bin = [bin / float(count) if count != 0 else bin for bin,
            count in zip(voting_bin, voting_count)]
        LOGGER.debug("{}".format(voting_bin))

        if sum(voting_bin) == 0:
            return (0, False)
        else:
            return (voting_bin.index(max(voting_bin)), True)


    def form_action_set(self, action):

        action_set = ActionSet(self)

        for cl in self.set:
            if (cl.action == action):
                action_set.set.append(cl)


        return action_set

    def update_set(self, curr_action):


        k_i = []
        sum_ks = 0
        for cl in self.set:
            
            cl.exp+=1
            
            correct = 0
            if cl.action == curr_action:
                correct = 1
                cl.corr_class +=1

            cl.acc += (correct - cl.acc) / float(cl.exp)

            if cl.action == curr_action:
                if cl.acc > cons.acc_0:
                    k_i.append(1)
                else:
                    k_i.append(cons.alpha * (cl.acc / cons.acc_0) ** cons.nu)
            else:
                k_i.append(0)

            sum_ks += k_i[-1] * cl.numerosity

        for (ind, cl) in enumerate(self.set):

            rel_k = (k_i[ind] * cl.numerosity) / float(sum_ks) 

            cl.F_macro = cl.F_macro + cons.beta * (rel_k - cl.F_macro)
            cl.F_micro = cl.F_macro / float(cl.numerosity)


        return


    #def update_set(self, curr_action):

    #    theta_acc = 2 * cons.theta_GA

    #

    #    for cl in self.set:

    #        cl.exp += 1

    #        action_ind = cons.env.all_classes.index(curr_action)
    #        cl.exp_i[action_ind] += 1


    #        corr = 0
    #        if cl.action == curr_action:
    #            corr = 1
    #            cl.corr_class += 1
    #            cl.corr_class_i[action_ind] += 1

    #        
    #        cl.acc_i[action_ind] = cl.corr_class_i[action_ind] / float(cl.exp_i[action_ind])
    #        #cl.acc_i[action_ind] = cl.corr_class_i[action_ind] / float(cl.exp)

    #        rule_cov = sum([1 for exp in cl.exp_i if exp > 1])

    #        sum_bin = 0
    #        for (acc, exp) in zip(cl.acc_i, cl.exp_i):

    #            if 1 < exp < theta_acc:
    #                w = exp / theta_acc
    #            elif exp >= theta_acc:
    #                cee = sum([1 for ex in cl.exp_i if ex >= theta_acc])
    #                sum_exp = sum([ex for ex in cl.exp_i if 1 < ex < theta_acc])
    #                w = ((rule_cov*theta_acc) - sum_exp) / (cee * theta_acc)
    #            elif exp == 1:
    #                w = 0
    #            else:
    #                w = 1

    #            sum_bin += acc * w



    #        sum_acc = sum([acc for (acc, exp) in zip(cl.acc_i, cl.exp_i) if exp > 1])
    #        #cl.cs_acc = (1/float(rule_cov)) * sum_acc
    #        cl.cs_acc = (1/float(rule_cov)) * sum_bin

    #        # update accuracy
    #        #cl.acc = cl.cs_acc
    #        cl.acc += (corr - cl.acc) / float(cl.exp)

    #        # update micro classifier fitness
    #        cl.F_micro = cl.acc**cons.nu

    #        # update macro classifier fitness
    #        cl.F_macro = cl.F_micro * cl.numerosity

    #    return


